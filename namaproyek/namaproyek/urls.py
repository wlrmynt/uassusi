"""namaproyek URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from . import views
from django.conf.urls import url




urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^about/$', views.about, name='about'),
    url(r'^pesanan/$', views.pesanan, name='pesanan'),
    url(r'^kontak/$', views.kontak, name='kontak'),
    url(r'^pemesanan/$', views.pemesanan, name='pemesanan'),
    url(r'^tambah/$', views.pemesanan, name='pemesanan'),
    url(r'^historyPemesanan/$', views.historyPemesanan, name='historyPemesanan'),
    
    url(r'^tambah_pesanan/$', views.tambah_pesanan, name='tambah_pesanan'),


]
