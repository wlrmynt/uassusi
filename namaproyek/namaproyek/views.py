from django.shortcuts import render,redirect
from django.http import JsonResponse,HttpResponse
import os
import json


def home(request):
    data_menu_makanan = [
         {
      "model": "namaproyek.menumakanan",
      "pk": 1,
      "fields": {
        "nama": "Ayam Kitchen",
        "img": "https://cdn.antaranews.com/cache/1200x800/2022/08/03/ayam-geprek.png",
        "stok": 120,
        "deskripsi": "Ayam Kitchen adalah hidangan lezat yang terkenal dengan rasa gurih dan renyahnya kulit ayam yang disajikan. Hidangan ini terbuat dari potongan ayam segar yang dipersiapkan dengan rempah-rempah.",
        "harga": 15000
      }
    },
    {
      "model": "namaproyek.menumakanan",
      "pk": 2,
      "fields": {
        "nama": "Cireng",
        "img": "https://bobobox.com/blog/wp-content/uploads/2019/09/cireng.jpg",
        "stok": 440,
        "deskripsi": "Cireng adalah camilan khas Indonesia yang terkenal dengan rasa gurih dan tekstur yang kenyal. Biasanya terbuat dari tepung kanji atau  epung aci yang dicampur dengan bahan-bahan lain.",
        "harga": 12000
      }
    },
    {
      "model": "namaproyek.menumakanan",
      "pk": 3,
      "fields": {
        "nama": "Nasi Liwet",
        "img": "https://blog.bankmega.com/wp-content/uploads/2022/10/Makanan-tradisional-indonesia.jpg",
        "stok": 370,
        "deskripsi": "Nasi Liwet adalah hidangan tradisional Indonesia yang terkenal dengan cita rasa yang kaya dan penggunaan bumbu-bumbu alami. Hidangan ini biasanya disajikan dalam satu wadah besar.",
        "harga": 18000
      }
    },
    {
      "model": "namaproyek.menumakanan",
      "pk": 4,
      "fields": {
        "nama": "Ayam Ceker",
        "img": "https://cdn.idntimes.com/content-images/post/20200929/120181674-191204059039796-2444371911129419281-n-fa8776034a2014198c5e559975feb6f5_600x400.jpg",
        "stok": 320,
        "deskripsi": "Ayam Ceker adalah hidangan yang terbuat dari bagian kaki ayam, yang dikenal dengan kelezatan daging dan gelatinositas kulitnya. Bagian kaki ayam ini sering dimasak menjadi hidangan yang lezat.",
        "harga": 18000
      }
    },
    {
      "model": "namaproyek.menumakanan",
      "pk": 5,
      "fields": {
        "nama": "Martabak Manis",
        "img": "https://www.dapurkobe.co.id/wp-content/uploads/martabak-8-rasa.jpg",
        "stok": 120,
        "deskripsi": "Martabak Manis adalah hidangan tradisional Indonesia yang memikat selera dengan kombinasi adonan lembut dan berbagai pilihan isian manis. Adonan martabak manis biasanya",
        "harga": 18000
      }
    },
    {
      "model": "namaproyek.menumakanan",
      "pk": 6,
      "fields": {
        "nama": "Natto",
        "img": "https://d1vbn70lmn1nqe.cloudfront.net/prod/wp-content/uploads/2022/12/19062216/mengenal-makanan-natto-yang-punya-banyak-manfaat-kesehatan-halodoc.jpg",
        "stok": 310,
        "deskripsi": "Natto adalah hidangan yang berasal dari Jepang, terkenal dengan bahan utamanya, yaitu biji kedelai yang difermentasi. Proses fermentasi ini melibatkan bakteri Bacillus subtilis natto.",
        "harga": 18000
      }
    }
    ];
    return render(request, 'home.html',{'data_menu_makanan': data_menu_makanan})

def about(request):
    return render(request, 'about.html')

def pesanan(request):
    data_menu_makanan = [
         {
      "model": "namaproyek.menumakanan",
      "pk": 1,
      "fields": {
        "nama": "Ayam Kitchen",
        "img": "https://cdn.antaranews.com/cache/1200x800/2022/08/03/ayam-geprek.png",
        "stok": 120,
        "deskripsi": "Ayam Kitchen adalah hidangan lezat yang terkenal dengan rasa gurih dan renyahnya kulit ayam yang disajikan. Hidangan ini terbuat dari potongan ayam segar yang dipersiapkan dengan rempah-rempah.",
        "harga": 15000
      }
    },
    {
      "model": "namaproyek.menumakanan",
      "pk": 2,
      "fields": {
        "nama": "Cireng",
        "img": "https://bobobox.com/blog/wp-content/uploads/2019/09/cireng.jpg",
        "stok": 440,
        "deskripsi": "Cireng adalah camilan khas Indonesia yang terkenal dengan rasa gurih dan tekstur yang kenyal. Biasanya terbuat dari tepung kanji atau  epung aci yang dicampur dengan bahan-bahan lain.",
        "harga": 12000
      }
    },
    {
      "model": "namaproyek.menumakanan",
      "pk": 3,
      "fields": {
        "nama": "Nasi Liwet",
        "img": "https://blog.bankmega.com/wp-content/uploads/2022/10/Makanan-tradisional-indonesia.jpg",
        "stok": 370,
        "deskripsi": "Nasi Liwet adalah hidangan tradisional Indonesia yang terkenal dengan cita rasa yang kaya dan penggunaan bumbu-bumbu alami. Hidangan ini biasanya disajikan dalam satu wadah besar.",
        "harga": 18000
      }
    },
    {
      "model": "namaproyek.menumakanan",
      "pk": 4,
      "fields": {
        "nama": "Ayam Ceker",
        "img": "https://cdn.idntimes.com/content-images/post/20200929/120181674-191204059039796-2444371911129419281-n-fa8776034a2014198c5e559975feb6f5_600x400.jpg",
        "stok": 320,
        "deskripsi": "Ayam Ceker adalah hidangan yang terbuat dari bagian kaki ayam, yang dikenal dengan kelezatan daging dan gelatinositas kulitnya. Bagian kaki ayam ini sering dimasak menjadi hidangan yang lezat.",
        "harga": 18000
      }
    },
    {
      "model": "namaproyek.menumakanan",
      "pk": 5,
      "fields": {
        "nama": "Martabak Manis",
        "img": "https://www.dapurkobe.co.id/wp-content/uploads/martabak-8-rasa.jpg",
        "stok": 120,
        "deskripsi": "Martabak Manis adalah hidangan tradisional Indonesia yang memikat selera dengan kombinasi adonan lembut dan berbagai pilihan isian manis. Adonan martabak manis biasanya",
        "harga": 18000
      }
    },
    {
      "model": "namaproyek.menumakanan",
      "pk": 6,
      "fields": {
        "nama": "Natto",
        "img": "https://d1vbn70lmn1nqe.cloudfront.net/prod/wp-content/uploads/2022/12/19062216/mengenal-makanan-natto-yang-punya-banyak-manfaat-kesehatan-halodoc.jpg",
        "stok": 310,
        "deskripsi": "Natto adalah hidangan yang berasal dari Jepang, terkenal dengan bahan utamanya, yaitu biji kedelai yang difermentasi. Proses fermentasi ini melibatkan bakteri Bacillus subtilis natto.",
        "harga": 18000
      }
    }
    ];
    return render(request, 'pesanan.html', {'data_menu_makanan': data_menu_makanan})

def kontak(request):
    return render(request, 'kontak.html')

def pemesanan(request):

    file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'pesanan.json')

    with open(file_path, 'r') as file:
      data_json = json.load(file)
    
    return render(request, 'pemesanan.html')

def historyPemesanan(request):
    file_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../pesanan.json')
    
    with open(file_path, 'r') as file:
      data_json = json.load(file)
    
    return render(request, 'pemesanan.html',{'data_json' : data_json})

def tambah_pesanan(request):
        if request.method == 'POST':   
            nama = request.POST.get('nama')
            makanan = request.POST.get('makanan')
            harga = request.POST.get('harga')
            qty =  request.POST.get('qty')
            harga = int(harga)
            qty = int(qty)
            total = harga * qty
            
            pesanan_data = {
                'nama': nama,
                'makanan'   :   makanan,
                'harga' : harga,
                'qty': qty,
                'total' :   total
            }
            file_path = 'pesanan.json'

            
            if not os.path.exists(file_path):
                with open(file_path, 'w') as file:
                    json.dump([], file)

            with open(file_path, 'r') as file:
                data_pesanan = json.load(file)

            data_pesanan.append(pesanan_data)

            with open(file_path, 'w') as file:
                json.dump(data_pesanan, file)

            return redirect('/pesanan')
        else:
            return HttpResponse('Terjadi Kesalahan Coba Kembali Lagi')